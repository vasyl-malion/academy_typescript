export interface IControls {
  PlayerOneAttack: string,
  PlayerOneBlock: string,
  PlayerTwoAttack: string,
  PlayerTwoBlock: string,
  PlayerTwoBlock2: string,
  PlayerOneCriticalHitCombination: string[],
  PlayerTwoCriticalHitCombination: string[]
}

export interface ICreateModal {
  title: string,
  bodyElement: string,
  onClose?: () => void
}

export interface IOptions {
  method: string
}

export interface IElementDOM {
  tagName: string,
  className?: string,
  attributes?: object,
}

export interface IFighters {
  _id: string,
  name: string,
  source: string
}

export interface IFightersDetails extends IFighters{
  health: number,
  attack: number,
  defense: number,
}

export interface IAttributes {
  src: string,
  title: string,
  alt: string
}