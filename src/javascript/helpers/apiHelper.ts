import { fightersDetails, fighters } from './mockData';
import { IFightersDetails, IFighters, IOptions } from '../interfaces/interfaces';

import { callApiReturn, fakeCallApi } from '../types/types';

const API_URL:string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callApi(endpoint: string, method: string): Promise<callApiReturn> {
  const url: string = API_URL + endpoint;
  const options: IOptions = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response: Response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error: object) => {
          throw error;
        });
}

// Promise<IFighters[] | IFightersDetails | never>
async function fakeCallApi(endpoint: string): Promise<fakeCallApi>{
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFightersDetails | undefined {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);

  return fightersDetails.find((it: IFightersDetails) => it._id === id);
}

export { callApi };
