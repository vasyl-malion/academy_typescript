import { IElementDOM } from '../interfaces/interfaces';

export function createElement({ tagName, className, attributes = {} } : IElementDOM): HTMLElement  {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: Array<string> = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key: string) => element.setAttribute(key, (<any>attributes)[key]));

  return element;
}
