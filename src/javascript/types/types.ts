import { IFighters, IFightersDetails } from '../interfaces/interfaces';

export type onClose = (() => void) | undefined;

export type callApiReturn = IFighters[] | IFightersDetails | never;

export type fakeCallApi = Promise<IFighters[] | IFightersDetails | never>;

export type selectedFighter = (event: Event, fighterId: string) => Promise<void>;