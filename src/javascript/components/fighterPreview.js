"use strict";
exports.__esModule = true;
exports.createFighterImage = exports.createFighterPreview = void 0;
var domHelper_1 = require("../helpers/domHelper");
function createFighterPreview(fighter, position) {
    var positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    var fighterElement = domHelper_1.createElement({
        tagName: 'div',
        className: "fighter-preview___root " + positionClassName
    });
    fighter ? fighterElement.append(createFighterImage(fighter), fighterInfo(fighter)) : null;
    return fighterElement;
}
exports.createFighterPreview = createFighterPreview;
function createFighterImage(fighter) {
    var source = fighter.source, name = fighter.name;
    var attributes = {
        src: source,
        title: name,
        alt: name
    };
    var imgElement = domHelper_1.createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes: attributes
    });
    return imgElement;
}
exports.createFighterImage = createFighterImage;
function oneFighterInfoFeature(key, value) {
    var feature = domHelper_1.createElement({
        tagName: "div",
        className: "fighter-preview__feature"
    });
    var keyOfFeature = domHelper_1.createElement({ tagName: "span" });
    keyOfFeature.innerHTML = key;
    var valueOfFeature = domHelper_1.createElement({
        tagName: "span",
        className: "fighter-preview__feature-value"
    });
    valueOfFeature.innerHTML = value;
    feature.append(keyOfFeature, valueOfFeature);
    return feature;
}
function fighterInfo(fighter) {
    var container = domHelper_1.createElement({
        tagName: "div",
        className: "fighter-preview__info-container"
    });
    var name = domHelper_1.createElement({
        tagName: "span",
        className: "fighter-preview__info-name"
    });
    var health = oneFighterInfoFeature("Health: ", fighter.health);
    var attack = oneFighterInfoFeature("Attack: ", fighter.attack);
    var defense = oneFighterInfoFeature("Defense: ", fighter.defense);
    name.innerHTML = fighter.name.toUpperCase();
    container.append(name, health, attack, defense);
    return container;
}
