"use strict";
exports.__esModule = true;
exports.renderArena = void 0;
var domHelper_1 = require("../helpers/domHelper");
var fighterPreview_1 = require("./fighterPreview");
var fight_1 = require("./fight");
var winner_1 = require("./modal/winner");
function renderArena(selectedFighters) {
    var root = document.getElementById('root');
    var arena = createArena(selectedFighters);
    root.innerHTML = '';
    root.append(arena);
    fight_1.fight.apply(void 0, selectedFighters).then(function (data) {
        winner_1.showWinnerModal(data);
    })["catch"](function (err) { return console.log(err); });
}
exports.renderArena = renderArena;
function createArena(selectedFighters) {
    var arena = domHelper_1.createElement({ tagName: 'div', className: 'arena___root' });
    var healthIndicators = createHealthIndicators.apply(void 0, selectedFighters);
    var fighters = createFighters.apply(void 0, selectedFighters);
    arena.append(healthIndicators, fighters);
    return arena;
}
function createHealthIndicators(leftFighter, rightFighter) {
    var healthIndicators = domHelper_1.createElement({ tagName: 'div', className: 'arena___fight-status' });
    var versusSign = domHelper_1.createElement({ tagName: 'div', className: 'arena___versus-sign' });
    var leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
    var rightFighterIndicator = createHealthIndicator(rightFighter, 'right');
    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
}
function createHealthIndicator(fighter, position) {
    var name = fighter.name;
    var container = domHelper_1.createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
    var fighterName = domHelper_1.createElement({ tagName: 'span', className: 'arena___fighter-name' });
    var indicator = domHelper_1.createElement({ tagName: 'div', className: 'arena___health-indicator' });
    var bar = domHelper_1.createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: position + "-fighter-indicator" } });
    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);
    return container;
}
function createFighters(firstFighter, secondFighter) {
    var battleField = domHelper_1.createElement({ tagName: 'div', className: "arena___battlefield" });
    var firstFighterElement = createFighter(firstFighter, 'left');
    var secondFighterElement = createFighter(secondFighter, 'right');
    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
}
function createFighter(fighter, position) {
    var imgElement = fighterPreview_1.createFighterImage(fighter);
    var positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
    var fighterElement = domHelper_1.createElement({
        tagName: 'div',
        className: "arena___fighter " + positionClassName
    });
    fighterElement.append(imgElement);
    return fighterElement;
}
