"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.getBlockPower = exports.getHitPower = exports.getSuperPower = exports.getDamage = exports.fight = void 0;
var controls_1 = require("../../constants/controls");
var firstFighterSuperAttackAvailable = true;
var secondFighterSuperAttackAvailable = true;
function fight(firstFighter, secondFighter) {
    return __awaiter(this, void 0, void 0, function () {
        var firstFighterHealth, secondFighterHealth, firstFighterBlockAvailable, secondFighterBlockAvailable, firstFighterLineHealth, secondFighterLineHealth;
        return __generator(this, function (_a) {
            firstFighterHealth = firstFighter.health;
            secondFighterHealth = secondFighter.health;
            firstFighterBlockAvailable = false;
            secondFighterBlockAvailable = false;
            firstFighterLineHealth = document.getElementById('left-fighter-indicator');
            secondFighterLineHealth = document.getElementById('right-fighter-indicator');
            return [2 /*return*/, new Promise(function (resolve) {
                    if (firstFighterSuperAttackAvailable) {
                        useSuperAttackPress.apply(void 0, __spreadArrays([function () { return useSuperAttack(firstFighter, secondFighter, secondFighterHealth, secondFighterLineHealth, 1); }], controls_1.controls.PlayerOneCriticalHitCombination));
                    }
                    if (secondFighterSuperAttackAvailable) {
                        useSuperAttackPress.apply(void 0, __spreadArrays([function () { return useSuperAttack(secondFighter, firstFighter, firstFighterHealth, firstFighterLineHealth, 2); }], controls_1.controls.PlayerTwoCriticalHitCombination));
                    }
                    document.addEventListener('keydown', function (e) {
                        switch (e.code) {
                            case controls_1.controls.PlayerOneBlock:
                                firstFighterBlockAvailable = true;
                                break;
                            case controls_1.controls.PlayerTwoBlock:
                                secondFighterBlockAvailable = true;
                                break;
                        }
                    });
                    document.addEventListener('keyup', function (e) {
                        switch (e.code) {
                            case controls_1.controls.PlayerOneBlock:
                                firstFighterBlockAvailable = false;
                                break;
                            case controls_1.controls.PlayerTwoBlock:
                                secondFighterBlockAvailable = false;
                                break;
                            case controls_1.controls.PlayerOneAttack:
                                playerAttack(firstFighter, secondFighter, secondFighterHealth, secondFighterLineHealth, firstFighterBlockAvailable, secondFighterBlockAvailable);
                                break;
                            case controls_1.controls.PlayerTwoAttack:
                                playerAttack(secondFighter, firstFighter, firstFighterHealth, firstFighterLineHealth, secondFighterBlockAvailable, firstFighterBlockAvailable);
                                break;
                        }
                        if (firstFighter.health <= 0) {
                            resolve(secondFighter);
                        }
                        else if (secondFighter.health <= 0) {
                            resolve(firstFighter);
                        }
                    });
                })];
        });
    });
}
exports.fight = fight;
var useSuperAttack = function (attacker, defender, fighterHealth, fighterLineHealth, superAttackAvailable) {
    var timeOutOfSuperAttack = 10000;
    if (superAttackAvailable === 1) {
        if (firstFighterSuperAttackAvailable) {
            minusHealth(attacker, defender, fighterHealth, fighterLineHealth);
            firstFighterSuperAttackAvailable = false;
            setTimeout(function () { return firstFighterSuperAttackAvailable = true; }, timeOutOfSuperAttack);
        }
    }
    else if (superAttackAvailable === 2) {
        if (secondFighterSuperAttackAvailable) {
            minusHealth(attacker, defender, fighterHealth, fighterLineHealth);
            secondFighterSuperAttackAvailable = false;
            setTimeout(function () { return secondFighterSuperAttackAvailable = true; }, timeOutOfSuperAttack);
        }
    }
};
var minusHealth = function (attacker, defender, fighterHealth, fighterLineHealth) {
    defender.health -= getSuperPower(attacker);
    getFighterHealth(defender, fighterHealth, fighterLineHealth);
};
var playerAttack = function (attacker, defender, fighterHealth, fighterLineHealth, attackerFighterBlockAvailable, defenderFighterBlockAvailable) {
    if (!attackerFighterBlockAvailable) {
        if (!defenderFighterBlockAvailable) {
            defender.health -= getDamage(attacker, defender);
        }
        getFighterHealth(defender, fighterHealth, fighterLineHealth);
    }
};
function getDamage(attacker, defender) {
    var damage = getHitPower(attacker) - getBlockPower(defender);
    return Math.max(0, damage);
}
exports.getDamage = getDamage;
function getSuperPower(fighter) {
    return fighter.attack * 2;
}
exports.getSuperPower = getSuperPower;
function getHitPower(fighter) {
    return fighter.attack * (1 + Math.random());
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    return fighter.defense * (1 + Math.random());
}
exports.getBlockPower = getBlockPower;
function getFighterHealth(fighter, initialFighterHealth, lineHealth) {
    var result = fighter.health / initialFighterHealth * 100;
    if (result > 0) {
        lineHealth.style.width = result + "%";
    }
    else {
        lineHealth.style.width = "0%";
    }
    return result;
}
function useSuperAttackPress(callback) {
    var codes = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        codes[_i - 1] = arguments[_i];
    }
    var pressed = new Set();
    var keysPress = function (event) {
        pressed.add(event.code);
        for (var _i = 0, codes_1 = codes; _i < codes_1.length; _i++) {
            var code = codes_1[_i];
            if (!pressed.has(code)) {
                return;
            }
        }
        pressed.clear();
        callback();
    };
    var keysDelete = function (event) {
        pressed["delete"](event.code);
    };
    document.addEventListener('keydown', keysPress);
    document.addEventListener('keyup', keysDelete);
}
