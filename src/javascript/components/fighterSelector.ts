import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
const versusImg =  require("../../../resources/versus.png");
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFightersDetails } from '../interfaces/interfaces';
import { selectedFighter, callApiReturn } from '../types/types';

export function createFightersSelector(): selectedFighter {

  let selectedFighters: Array<IFightersDetails> = [];

  return async (event, fighterId): Promise<void> => {
    const fighter: callApiReturn = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: IFightersDetails = playerOne ?? fighter as IFightersDetails;
    const secondFighter: IFightersDetails | callApiReturn =
      Boolean(playerOne) ? playerTwo ?? fighter as IFightersDetails : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<callApiReturn> {
  const fighterDet: callApiReturn = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(fighterId, fighterDet);

  return fighterDet;
}

function renderSelectedFighters(selectedFighters: IFightersDetails[]) {
  const fightersPreview: Element = document.querySelector('.preview-container___root')!;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFightersDetails[]): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = (): void => startFight(selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFightersDetails[]) {
  renderArena(selectedFighters);
}
