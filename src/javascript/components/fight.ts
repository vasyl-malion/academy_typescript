import { controls } from '../../constants/controls';
import { IFightersDetails} from '../interfaces/interfaces';

let firstFighterSuperAttackAvailable: boolean = true
let secondFighterSuperAttackAvailable: boolean = true

export async function fight(firstFighter: IFightersDetails, secondFighter: IFightersDetails): Promise<IFightersDetails> {

  const { health: firstFighterHealth} = firstFighter
  const { health: secondFighterHealth} = secondFighter

  let firstFighterBlockAvailable: boolean = false
  let secondFighterBlockAvailable: boolean = false

  const firstFighterLineHealth: HTMLElement = document.getElementById('left-fighter-indicator')!
  const secondFighterLineHealth: HTMLElement = document.getElementById('right-fighter-indicator')!

  return new Promise((resolve) => {

    if (firstFighterSuperAttackAvailable) {
      useSuperAttackPress( () => useSuperAttack(
        firstFighter,
        secondFighter,
        secondFighterHealth,
        secondFighterLineHealth,
        1),
        ...controls.PlayerOneCriticalHitCombination
      )
    }

    if (secondFighterSuperAttackAvailable) {
      useSuperAttackPress( () => useSuperAttack(
        secondFighter,
        firstFighter,
        firstFighterHealth,
        firstFighterLineHealth,
        2),
        ...controls.PlayerTwoCriticalHitCombination)
    }

    document.addEventListener('keydown', function(e: KeyboardEvent) {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterBlockAvailable = true
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlockAvailable = true
          break;
      }
    });

    document.addEventListener('keyup', function(e: KeyboardEvent) {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterBlockAvailable = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlockAvailable = false;
          break;
        case controls.PlayerOneAttack:
          playerAttack(
            firstFighter,
            secondFighter,
            secondFighterHealth,
            secondFighterLineHealth,
            firstFighterBlockAvailable,
            secondFighterBlockAvailable
          )
          break
        case controls.PlayerTwoAttack:
          playerAttack(
            secondFighter,
            firstFighter,
            firstFighterHealth,
            firstFighterLineHealth,
            secondFighterBlockAvailable,
            firstFighterBlockAvailable
          )
          break;
      }

      if (firstFighter.health <= 0 ) {
        resolve(secondFighter)
      } else if (secondFighter.health <= 0) {
        resolve(firstFighter)
      }
    })
  })
}

const useSuperAttack = (attacker: IFightersDetails,
                        defender: IFightersDetails,
                        fighterHealth: number,
                        fighterLineHealth: HTMLElement,
                        superAttackAvailable: number) => {

  const timeOutOfSuperAttack: number = 10000;

  if (superAttackAvailable === 1) {
    if (firstFighterSuperAttackAvailable) {
      minusHealth(attacker, defender, fighterHealth, fighterLineHealth)
      firstFighterSuperAttackAvailable = false
      setTimeout(() => firstFighterSuperAttackAvailable = true, timeOutOfSuperAttack)
    }
  } else if (superAttackAvailable === 2) {
    if (secondFighterSuperAttackAvailable) {
      minusHealth(attacker, defender, fighterHealth, fighterLineHealth)
      secondFighterSuperAttackAvailable = false
      setTimeout(() => secondFighterSuperAttackAvailable = true, timeOutOfSuperAttack)
    }
  }
}

const minusHealth = (attacker: IFightersDetails,
                     defender: IFightersDetails,
                     fighterHealth: number,
                     fighterLineHealth: HTMLElement): void => {

  defender.health -= getSuperPower(attacker)
  getFighterHealth(defender, fighterHealth, fighterLineHealth)
}

const playerAttack = (attacker: IFightersDetails,
                      defender: IFightersDetails,
                      fighterHealth: number,
                      fighterLineHealth: HTMLElement,
                      attackerFighterBlockAvailable: boolean,
                      defenderFighterBlockAvailable: boolean): void => {
  if (!attackerFighterBlockAvailable) {

    if(!defenderFighterBlockAvailable) {
      defender.health -= getDamage(attacker, defender)
    }

    getFighterHealth(defender, fighterHealth, fighterLineHealth)
  }
}

export function getDamage(attacker: IFightersDetails, defender: IFightersDetails): number {
  let damage: number = getHitPower(attacker) - getBlockPower(defender)
  return Math.max(0, damage)
}

export function getSuperPower(fighter: IFightersDetails): number {
  return fighter.attack * 2
}

export function getHitPower(fighter: IFightersDetails): number {
  return fighter.attack * (1 + Math.random())
}

export function getBlockPower(fighter: IFightersDetails): number {
  return fighter.defense * (1 + Math.random())
}

function getFighterHealth(fighter: IFightersDetails, initialFighterHealth: number, lineHealth: HTMLElement): number {
  const result: number  = fighter.health / initialFighterHealth * 100
  if (result > 0) {
    lineHealth.style.width = `${result}%`
  } else {
    lineHealth.style.width = `0%`
  }
  return result;
}

function useSuperAttackPress(callback: () => void, ...codes: string[]): void {

  let pressed: Set<string> = new Set();

  const keysPress = (event: KeyboardEvent) =>  {
    pressed.add(event.code)

    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear()

    callback()
  };

  const keysDelete = (event: KeyboardEvent) => {
    pressed.delete(event.code);
  }

  document.addEventListener('keydown',keysPress);

  document.addEventListener('keyup', keysDelete);
}