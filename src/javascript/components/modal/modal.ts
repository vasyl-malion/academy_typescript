import { createElement } from '../../helpers/domHelper';
import { ICreateModal } from '../../interfaces/interfaces';
import { onClose } from '../../types/types';

export function showModal({ title, bodyElement, onClose = () => {} }: ICreateModal): void {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose }); 
  
  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root')!;
}

function createModal({ title, bodyElement, onClose }: ICreateModal): HTMLElement {
  const layer: HTMLElement = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer: HTMLElement = createElement({ tagName: 'div', className: 'modal-root' });
  const header: HTMLElement = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: onClose): HTMLElement {
  const headerElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement: HTMLElement = createElement({ tagName: 'span' });
  const closeButton: HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = (): void => {
    hideModal();
    if (onClose) {
      onClose();
    }
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal: Element = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
