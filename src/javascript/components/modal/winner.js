"use strict";
exports.__esModule = true;
exports.showWinnerModal = void 0;
var modal_1 = require("./modal");
function showWinnerModal(fighter) {
    modal_1.showModal({
        title: fighter.name + " won!",
        bodyElement: ''
    });
}
exports.showWinnerModal = showWinnerModal;
