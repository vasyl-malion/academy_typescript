import {showModal } from './modal';
import { IFighters } from '../../interfaces/interfaces';

export function showWinnerModal(fighter: IFighters): void {

  showModal({
    title: `${fighter.name} won!`,
    bodyElement: ''
  })
}