import { callApi } from '../helpers/apiHelper';
import { callApiReturn } from '../types/types';

class FighterService {
  async getFighters(): Promise<callApiReturn> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: callApiReturn = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<callApiReturn> {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      return await callApi(endpoint, 'GET');

    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
